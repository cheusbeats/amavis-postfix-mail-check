#!/bin/bash
post_addres=$1 #Storing the parameter value into a variable

#Checking if this mail is in the Amavis list, if not, then add and restart the amavis service

test_amavis=$(grep -E "^$post_addres$" -i -c /etc/amavis/whitelist)

if [ "$test_amavis" -gt 0 ]
then echo "Данный адрес уже присутствует в белом списке amavis"
else
        echo "Добавляем адрес в белый список amavis"
        echo -e "$post_addres\n" >> /etc/amavis/whitelist #Add mail to the amavis whitelist
        service amavis restart #Restarting the amavis service
fi

#Checking if the Postfix list contains this mail, if not, add it

str_postfix_find="$post_addres  OK" #Generate search string
test_postfix=$(grep -E "^$str_postfix_find$" -i -c /etc/postfix/sender_access)

if [ "$test_postfix" -gt 0 ]
then echo "Данный адрес уже присутствует в белом списке Postfix"
else
        echo "Добавляем адрес в белый список postfix"
        echo -e "$str_postfix_find\n" >> /etc/postfix/sender_access #Add mail to postfix whitelist
        service postfix reload #Updating postfix service settings
fi

service amavis status #Check amavis service status
service postfix status #Check postfix service status