# Amavis-Postfix mail check

Script for searching and adding email addresses in whitelist (Amavis) and sender_access (Postfix) files
It is also possible to adapt for any other search in any other file by mask.

To install, move to /usr/local/bin

Launch:
/usr/local/bin/post_add.sh *email*